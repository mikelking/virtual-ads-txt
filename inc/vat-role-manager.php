<?php

/**
 * Class Role_Manager
 *
 * @see https://developer.wordpress.org/plugins/users/roles-and-capabilities/
 */
class VAT_Role_Manager extends Role_Manager_Base {
	const ROLE_NAME = 'Ad Manager';
	const ROLE_SLUG = 'ad_manager';
	const CAPE_NAME = 'edit_ads';
	const PRIORITY  = 11;
}

