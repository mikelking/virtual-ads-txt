<?php

/**
 * This is a base class and should not be directly instantiated
 * May convert to an abstract
 *
 * @see https://pippinsplugins.com/adding-custom-meta-fields-to-taxonomies/
 *
 * @see https://developer.wordpress.org/reference/functions/get_term_meta/
 * @see https://developer.wordpress.org/reference/functions/add_term_meta/
 * @see https://developer.wordpress.org/reference/functions/update_term_meta/
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/date
 */
class Taxonomy_Custom_Field_Base {
    const FIELD_LABEL = 'Field Label';
    const FIELD_WIDTH = 200;
    const TAXONOMY_ID = 'taxonomy';
    const FIELD_NAME  = 'FieldName'; // camelCase term meta name @ref Jira_Data_Consumer->set_sprint_taxonomy
    const PATTERN     = '';
    const UNIQUE      = true;

    private $field_data;
    private $term;

    public function __construct() {
        add_action( static::TAXONOMY_ID . '_edit_form_fields', [ $this, 'edit_meta_field' ], 10, 2 );
        add_action( static::TAXONOMY_ID .  '_add_form_fields', [ $this, 'add_new_meta_field' ], 10, 2 );
        add_action( 'edited_' . static::TAXONOMY_ID , [ $this, 'save_custom_field_data' ], 10, 2 );
        add_action( 'create_' . static::TAXONOMY_ID , [ $this, 'save_custom_field_data' ], 10, 2 );

    }

    public function get_pattern() {
        print( static::PATTERN );
    }

    /**
     *
     */
    public function get_custom_field_id() {
        print( 'term_meta[' . static::FIELD_NAME . ']' );
    }

    /**
     *
     */
    public function get_form_name() {
        print( static::FIELD_NAME );
    }

    /*
     * Add custom meta field to junction taxonomy for Junction Hub Page headline
     * Add term page
     */
    public function add_new_meta_field() {
        // this will add the custom meta field to the add new term page
        ?>
        <div class="form-field <?php $this->get_form_name(); ?>">
            <label for="<?php $this->get_custom_field_id(); ?>"><?php _e( static::FIELD_LABEL, 'field' ); ?></label>
            <input pattern="<?php $this->get_pattern(); ?>" type="text" width="<?php $this->get_field_width(); ?>" name="<?php $this->get_custom_field_id(); ?>" id="<?php $this->get_custom_field_id(); ?>" value="">
        </div>
        <?php
    }

    /**
     * @param $term_id
     * @return false|mixed|string
     */
    public function get_field_data( $term_id ) {
        $default = '';
        $this->field_data = get_term_meta( $term_id, static::FIELD_NAME );
        if ( isset( $this->field_data ) && is_array( $this->field_data ) ) {
            return $this->field_data[0];
        }
        return $default;
    }

    public function get_field_width() {
        print( static::FIELD_WIDTH );
    }

    /**
     * Edit taxonomy meta
     * @param $term
     */
    public function edit_meta_field( $term ) {
        $this->term = $term;
        // retrieve the existing value for this meta field.
        $this->get_field_data( $this->term->term_id );
        ?>
        <tr class="form-field <?php $this->get_form_name(); ?>">
            <th scope="row" valign="top"><label for="<?php $this->get_custom_field_id(); ?>"><?php _e( static::FIELD_LABEL, 'field' ); ?></label></th>
            <td>
                <input pattern="<?php $this->get_pattern(); ?>" class="<?php $this->get_form_name(); ?>" width="<?php $this->get_field_width(); ?>" type="text" name="<?php $this->get_custom_field_id(); ?>" id="<?php $this->get_custom_field_id(); ?>" value="<?php print( $this->get_field_data( $this->term->term_id ) ); ?>">
            </td>
        </tr>
        <?php
    }

    /**
     * This is an alternative to the klunky save_taxonomy_custom_meta() method
     *
     * @param $term_id
     *
     * @see https://developer.wordpress.org/reference/functions/update_term_meta/
     */
    public function save_custom_field_data( $term_id ) {
        $this->field_data = get_term_meta( $term_id, static::FIELD_NAME );
        if ( isset( $_POST['term_meta'] ) && array_key_exists( static::FIELD_NAME, $_POST['term_meta'] )) {
            update_term_meta( $term_id, static::FIELD_NAME, $_POST['term_meta'][static::FIELD_NAME] );
        }
    }
}
