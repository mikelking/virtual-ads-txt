<?php

class Log_Magick {
	const FILE_SPEC = __FILE__;
	const DATE_FMT     = "Y-m-d H:i:s";
	const DEFAULT_PATH = "_wpeprivate";

	private string $logfile;

	public function __construct( string $logfile ) {
		$this->logfile = $logfile;
	}

	public function dummy(): void {
		$defaultLogPath = ABSPATH . "/" . self::DEFAULT_PATH;
		$assoc_args = [];
		$defaultLogPath = ABSPATH . "/" . self::DEFAULT_PATH;
		if ( array_key_exists( 'log', $assoc_args ) ) {
			$this->log = $assoc_args['log'];
			$path_parts = pathinfo( $this->log );
			/** may need to build a logging class which would be returned as an object by this method.
			 * @see https://make.wordpress.org/cli/handbook/references/internal-api/wp-cli-log/
			 */
			$currentDir = pathinfo( dirname( __FILE__ ) );
			echo( " currentDir: " . print_r( $currentDir, true ) . PHP_EOL );
			if ( $currentDir['basename'] === $currentDir['filename'] ) {
				$dirParts = explode( '/', $currentDir['dirname'] );
				echo( " dirParts: " . print_r( $dirParts, true ) . PHP_EOL );
				foreach ( $dirParts as $key => $dirPart ) {
					if ( $dirPart === "plugins" ) {
						$pluginName = $dirParts[ $key + 1 ];
					}
				}
				echo( " pluginName: " . print_r( $pluginName, true ) . PHP_EOL );
			}

			$pluginDir = pathinfo( plugin_dir_path( __FILE__ ) );
			echo( " pluginDir: " . print_r( $pluginDir, true ) . PHP_EOL );
			$pluginName = "";
			$pluginParts = explode( '/'. $pluginDir['dirname'] );
			echo( " pluginParts: " . print_r( $pluginParts, true ) . PHP_EOL );
			foreach ( $pluginParts as $key => $pluginPart ) {
				if ( $pluginPart === "plugins" ) {
					$pluginName = $pluginParts[ $key + 1 ];
				}
			}
			echo( " pluginName: " . print_r( $pluginName, true ) . PHP_EOL );


			if ( defined( 'ABSPATH' ) ) {
				echo(" sitePath: " . ABSPATH . PHP_EOL);
				$deafultPath = pathinfo( ABSPATH );
				echo(" sitePath: " . print_r( $deafultPath, true ) . PHP_EOL);
				if ( file_exists( $defaultLogPath ) ) {
					$logDir = pathinfo( $defaultLogPath );
					echo(" logPathDir: " . print_r( $logDir, true) . PHP_EOL);
				} else {
					echo " logPathDir: not found" . PHP_EOL;
				}
			}

			echo( " Path: " . print_r( $path_parts, true ) . PHP_EOL );

			if ( file_exists( $path_parts['dirname'] ) ) {
				echo( "Path exist: " . $currentDir . PHP_EOL );
			}

		}
	}

}
