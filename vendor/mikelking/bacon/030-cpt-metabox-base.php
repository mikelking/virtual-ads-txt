<?php


/**
 * CPT_Metabox_Base - currently only setup a single metabox with an array of custom read only fields
 */
class CPT_Metabox_Base {
	const EDIT_METABOX_CAPE = false;
	const TEXT_DOMAIN = 'text-domain';
	const SCREEN_TYPES = [ 'post' ];

	private $custom_fields;
	private $metabox_name;

	public function __construct( $metabox_name, $custom_fields ) {
		$this->metabox_name = $metabox_name;
		$this->custom_fields = $custom_fields;

		/* Fire our meta box setup function on the post editor screen. */
		add_action( 'load-post.php', [ $this, "setup_metabox" ] );
		add_action( 'load-post-new.php', [ $this, "setup_metabox" ] );

	}

	/**
	 *
	 */
	public function setup_metabox() {
		add_action( 'add_meta_boxes', [ $this, "register_custom_field" ] );
	}

	/**
	 *
	 */
	public function register_custom_field() {
		add_meta_box(
			$this->metabox_name,
			__( $this->metabox_name, static::TEXT_DOMAIN ),
			[ $this, "render_metabox" ],
			static::SCREEN_TYPES,
			'normal'
		);
	}

	/**
	 * @param $post
	 */
	public function render_metabox( $post ) {
		$metabox = [];
		$metabox['post'] = $post;
		$metabox['label'] = __( $this->metabox_name, static::TEXT_DOMAIN );
		$metabox['class_nonce'] = $metabox['label'] . '_class_nonce';
		$metabox['id'] = $this->metabox_name . '_' . static::SCREEN_TYPES[0];
		$metabox['data'] = get_post_meta( $metabox['post']->ID, $this->metabox_name, true );
		$metabox['custom_fields'] = $this->custom_fields;

		/**
		 * This construct allows forking to a decorator pattern class
		 */
		if ( self::EDIT_METABOX_CAPE ) {
			wp_nonce_field( basename( __FILE__ ), $metabox['class_nonce'] );
			$output = '<label for="' . $metabox['id'] . '" >' . $metabox['label'] . '</label><br />';
			$output .= '<input type="text" name="' . $metabox['id'] . '" id="' . $metabox['id'] . '" value="' . esc_attr( $metabox['data'] ) . '" />';
			$output .= '';
		} else {
			// send $metabox to the decorator
			$output = '<table class="form-table">' . PHP_EOL;
			foreach ( $this->custom_fields as $custom_field ) {
				$output .= $custom_field->render_custom_field( $post );
			}
		}
		$output .= '</table>' . PHP_EOL;

		/* Display the post meta box. */
		print( $output );
	}

	public function render_custom_field_data( $data ) {
		if ( !empty( $data ) ) {
			foreach ( $data as $key => $value ) {
				printf( '<p><strong>%s</strong></p>', esc_html( $key ) );
				printf( '<p>%s</p>', esc_html( $value ) );
			}
		}
	}

	/**
	 * @param null $data
	 */
	public function update_custom_field( $data = null ) {
		if ( is_empty( $data ) ) {
			$data = "";
		}
	}
}
