<?php return array(
    'root' => array(
        'name' => 'mikelking/virtual-ads-txt',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => '9d3035e105ae23ae656ee91f9ccfce5bbb2141f3',
        'type' => 'wordpress-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'composer/installers' => array(
            'pretty_version' => 'v1.11.0',
            'version' => '1.11.0.0',
            'reference' => 'ae03311f45dfe194412081526be2e003960df74b',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/./installers',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'mikelking/bacon' => array(
            'pretty_version' => '1.2.11',
            'version' => '1.2.11.0',
            'reference' => 'd902b7df72ddd6b7c240864deeb575186ae66f36',
            'type' => 'wordpress-muplugin',
            'install_path' => __DIR__ . '/../mikelking/bacon',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'mikelking/virtual-ads-txt' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '9d3035e105ae23ae656ee91f9ccfce5bbb2141f3',
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'roundcube/plugin-installer' => array(
            'dev_requirement' => true,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'shama/baton' => array(
            'dev_requirement' => true,
            'replaced' => array(
                0 => '*',
            ),
        ),
    ),
);
