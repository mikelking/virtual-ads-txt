<?php
/*
Plugin Name: Virtual Ads.txt
Version: 1.2.0
Description: Edit, maintain and serve the content for your ads.txt in a WordPress content container.
Author: Mikel King
Text Domain: virtual-ads-txt
License: BSD(3 Clause)
License URI: http://opensource.org/licenses/BSD-3-Clause

	Copyright (C) 2014, Mikel King, olivent.com, (mikel.king AT olivent DOT com)
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

		* Redistributions of source code must retain the above copyright notice, this
		list of conditions and the following disclaimer.

		* Redistributions in binary form must reproduce the above copyright notice,
		this list of conditions and the following disclaimer in the documentation
		and/or other materials provided with the distribution.

		* Neither the name of the {organization} nor the names of its
		contributors may be used to endorse or promote products derived from
		this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * Helps inhibit cross site scripting attacks
 */
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

require( 'inc/dependencies.php' );
require( 'inc/vat-manager-admin.php' );
require( 'inc/vat-role-manager.php' );

/**
 * Class Virtual_Ads_Txt_Controller
 *
 * Purpose to act as the central hub for all ads.txt operations
 *
 * @todo Implement a site 2 site ads.txt import
 */
class Virtual_Ads_Txt_Controller extends Base_Plugin {
	const VERSION      = '1.2.0';
	const FILE_SPEC    = __FILE__;
	const PRIORITY     = 0;
	const HEADER       = "###\n# Ads.txt - created by the Virtual Ads.txt WordPress plugin. \n";
	const HEADER2      = "# Source: https://www.wordpress.org/plugins/virtual-ads-txt/\n#\n";
	const SPEC_SITE    = "# IAW IAB specifications https://iabtechlab.com/ads-txt/\n\n";
	const VALIDATOR    = "# VALIDATOR: https://adstxt.adnxs.com/ \n\n";
	const FOOTER       = "\n\n# TBD \n";
	const HTTP_STATUS  = 'Status: 200 OK';
	const HTTP_CODE    = 200;
	const CONTENT_TYPE = 'text/plain; charset=utf-8';
	const CACHE_CNTRL  = 'Cache-Control: max-age=';
	const CACHE_AGE    = 300; // duration in seconds
	const OPT_NAME     = 'virtual-ads-txt';
	const URI_TARGET   = 'ads.txt';

	public $options;
	public string $url;
	public $admin;
	public $debug_headers;

	protected function __construct() {
		$this->validate_standard_paths();

		// This is how to add a deactivation hook if needed
		register_deactivation_hook( static::FILE_SPEC, array( $this, 'deactivator' ) );

		// This is how to add an activation hook if needed
		register_activation_hook( static::FILE_SPEC, array( $this, 'activator' ) );

		if ( $this->is_ads_txt_request() ) {
			add_action( 'send_headers', array( $this, 'render_ads_txt' ), static::PRIORITY );
		}

		if ( is_admin() ) {
			$admin = new Virtual_Ads_Txt_Admin();
		}
	}

    /**
     * Renders the ads.txt file.
     *
     * This method is responsible for rendering the ads.txt file by sending the appropriate HTTP headers
     * and the contents of the ads.txt file.
     *
     * @return void
     */
    public function render_ads_txt(): void {
		$hhc = new HTTP_Header_Controller;
		$hhc->ob_begin();
		$this->send_http_page_headers();
		$this->send_ads_txt();
		$hhc->ob_flush();
	}

	public function deactivator(): void {
		$options = $this->get_options();
		if ( $options['remove_settings'] ) {
			delete_option( self::OPT_NAME );
		}
        $vatrm = new VAT_Role_Manager();
		$vatrm->remove_capabilities_and_roles();
	}

	public function activator(): void {
		$vatrm = new VAT_Role_Manager();
        $vatrm->add_capability_to_admin();
        $vatrm->add_custom_role();
	}

    /**
     * Sets the Content-Type header and returns the updated headers array.
     *
     * @param array $headers The current headers array.
     * @return array Returns the updated headers array with the Content-Type header set.
     */
    public function set_content_type( $headers ): array {
		if ( ! isset( $headers ) ) {
			print( 'Danger, danger! Will Robinson!\n' );
		}
		$headers['Content-Type'] = self::CONTENT_TYPE;
		$this->debug_headers = $headers;
		return( $headers );
	}

    /**
     * Sends the HTTP page headers for the current request.
     */
    public function send_http_page_headers(): void {
			header( 'Cache-Control: max-age=' . static::CACHE_AGE, true );
			//header( 'Content-Disposition: attachment; filename="ads.txt"', true );
			header( 'Content-Type:' . self::CONTENT_TYPE, true );
			header( 'Source: Virtual Ads.txt by Mikel King', true );
			header( self::HTTP_STATUS, true, self::HTTP_CODE );
	}

    /**
     * Determines whether the current request is an ads.txt request.
     *
     * @return bool Returns true if the current request is an ads.txt request, false otherwise.
     */
    public function is_ads_txt_request(): bool {
		$url = new URL_Magick();
        $uri = $url::$uri ?? "";
		if ( strcasecmp( trim( $uri, '/' ), self::URI_TARGET ) == 0 ) {
			return( true );
		}
		return( false );
	}

    /**
     * Sends the ads.txt content to the client.
     *
     * This method generates and outputs the complete ads.txt content to the client.
     * It includes the header, site-specific information, validator, dynamically generated ads.txt,
     * and the footer. Additionally, it also prints the debug headers and exits the script.
     *
     * @return void
     */
    public function send_ads_txt(): void {
		$output  = self::HEADER;
		$output .= self::HEADER2;
		$output .= self::SPEC_SITE;
		$output .= self::VALIDATOR;
		$output .= $this->get_virtual_ads_txt();
		$output .= self::FOOTER;

		print( $output );
		print_r( $this->debug_headers );
		exit;
	}

    /**
     * Get the virtual ads.txt content.
     *
     * @return string Returns the virtual ads.txt content as a string.
     */
    public function get_virtual_ads_txt (): string {
        $adsTxt = "";
		$this->get_options();
		if ( $this->options[self::OPT_NAME] != $adsTxt ) {
			$adsTxt = stripcslashes( $this->options[self::OPT_NAME] );
		}
        return $adsTxt;
	}

	/**
	 * Fastest array filter solution
	 * Ensures all data elements in the array ahve been
	 * sanitized or validated as appropriate to the filter map.
	 * @param $options
	 * @return array
	 *
	 */
	public function filter_options( $options ): array {
		$filter_map = array(
			self::OPT_NAME => FILTER_SANITIZE_STRING,
			'remove_settings' => FILTER_VALIDATE_BOOLEAN,
		);

		$this->options = filter_var_array( $options, $filter_map );
		return( $this->options );
	}


	/**
	 * Simply dig up the options and return that array
	 * @return array
	 */
	public function get_options() {
		$options = get_option( self::OPT_NAME );
		if ( ! is_array( $options ) ) {
			$this->set_default_options();
			return( $this->options );
		}

		$this->filter_options( $options );
		return( $this->options );
	}

    /**
     * Sets the default options for the class.
     *
     * @return array The array of default options that have been set.
     */
	public function set_default_options(): array {
		$this->options = [
			self::OPT_NAME => "# sample source: https://support.google.com/dfp_premium/answer/7441288?hl=en\n"
				. "google.com, pub-0000000000000000, DIRECT, f08c47fec0942fa0\n"
				. "google.com, pub-0000000000000000, RESELLER, f08c47fec0942fa0\n"
				. "greenadexchange.com, 12345, DIRECT, AEC242\n"
				. "blueadexchange.com, 4536, DIRECT\n"
				. "silverssp.com, 9675, RESELLER\n",
			'remove_settings' => false,
		];
		update_option( self::OPT_NAME, $this->options );
		return( $this->options );
	}


	public function init() {
		$this->validate_standard_paths();
	}

	/**
	 * Validate may not exactly be the correct term here but in essence
	 * we want to ensure that WordPress is setup so that we can work.
	 */
	public function validate_standard_paths(): void {
		if ( ! defined( 'WP_PLUGIN_URL' ) ) {
			if ( ! defined( 'WP_CONTENT_DIR' ) ) {
				define( 'WP_CONTENT_DIR', ABSPATH.'wp-content' );
			}
			if ( ! defined( 'WP_CONTENT_URL' ) ) {
				define( 'WP_CONTENT_URL', get_option( 'siteurl' ).'/wp-content' );
			}
			if ( ! defined( 'WP_PLUGIN_DIR' ) ) {
				define( 'WP_PLUGIN_DIR', WP_CONTENT_DIR.'/plugins' );
			}
			define( 'WP_PLUGIN_URL', WP_CONTENT_URL.'/plugins' );
		}
	}
}

$vatc = Virtual_Ads_Txt_Controller::get_instance();

